<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		version="1.0">
  <xsl:output method="text" />

  <xsl:template match="/">
    <xsl:text>@prefix ti: &lt;http://ns.ottr.xyz/template-instances.owl#&gt; .
    </xsl:text>
    <xsl:apply-templates />
  </xsl:template>

  <xsl:template match="instance">
    <!--  create template instance -->
    <xsl:text>[] ti:template </xsl:text>
    <xsl:text>&lt;</xsl:text><xsl:value-of select="../@template" /><xsl:text>&gt; ;</xsl:text>
    
    <!-- list values -->
    <xsl:text>   ti:values ( </xsl:text>
    <xsl:for-each select="*">
      <!-- value list item -->
      <xsl:text>&lt;</xsl:text><xsl:value-of select="text()" /><xsl:text>&gt;</xsl:text>
      <xsl:text> </xsl:text>
    </xsl:for-each>
    <xsl:text>) .</xsl:text>
  </xsl:template>

</xsl:stylesheet>
